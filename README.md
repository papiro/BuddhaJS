# BuddhaJS
----------------------
```
The Middle Way - the way of the Buddha - is the way of moderation.

A way apart from indulgence in sense-pleasures and apart from self-mortification.

When engaged in art, one is free from these things.  

BuddhaJS will be simple, so you can focus on your art and let BuddhaJS provide a personal platform for sharing it.

$ sudo npm install -g buddhajs
$ cd ~/workspace
$ buddhajs init
```
