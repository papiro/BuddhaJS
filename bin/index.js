#!/usr/bin/env node

'use strict'

var cp = require("child_process")
,	path = require("path")

,  cwd = process.cwd()

,  args = process.argv.slice(2)

// "buddhajs" = "buddhajs init"
if(~args.indexOf('init') || !args.length){
	let bootstrap = cp.spawn(path.resolve(__dirname, "Boot_strap.sh"));

	bootstrap.stdout.on("data", function(stdout){
		console.log(stdout.toString());
	});
	bootstrap.stderr.on("data", function(stderr){
		console.error(stderr.toString());
	});
	bootstrap.on("exit", function(code){
		if(code===0){
    		require("./init")(function(err){
    			if(err){
    				console.error(err.stack)
    				process.exit(1)
    			}
    		});
    	} else {
    		console.error("buddhajs failed to initialize")
    		process.exit(1);
    	}
	});
}

~args.indexOf('onboard') && spinUp()

function spinUp(){
    let onboard = require(`${cwd}/.backStage/onboard`)
    
    onboard(`${cwd}/pages/index.html`)
}
