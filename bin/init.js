var backStageRepo = `${process.cwd()}/.backStage`

,   fs = require("fs")
,   path = require("path")

,   toolfs = require(`${backStageRepo}/BuddhaJS/node_modules/tool.fs`)

module.exports = function(callback){
    var noop = function(){}
    ,   callback = callback || noop
/** 
 * Clone the buddhajs project into a hidden folder inside of
 *  the current working directory and now creates sym links to the files 
 *  needed so any edits made to those files will be mirrored "back-stage".
 *  The .backStage directories are also git repositories which are upstreamed
 *  to the projects on Github.  This is to provide parallel development of several
 *  projects simultaneously.
**/

    toolfs.mkdirTree({ js : {}, pages : {} }, function(err){
        if(err) return callback(err)
        toolfs.mklink(`${backStageRepo}/grate.css`, './css', function(err){
            if(err) return callback(err)
            toolfs.mklink(`${backStageRepo}/BuddhaJS/assets/index.html`, './pages/index.html', function(err){
                if(err) return callback(err)
                    toolfs.mklink(`${backStageRepo}/onboard`, './onboard', callback)
            })            
        })
    })    
}
