#!/bin/sh

BACKSTAGE=`pwd`/.backStage

cleanup(){
	cd ../..
	rm -rf .backStage/
	exit 1
}

exists(){
	command -v $1 2>&1 >/dev/null && return 0 || return 1
}

if exists git; then
	mkdir .backStage
	git clone git@github.com:papiro/BuddhaJS.git ./.backStage/BuddhaJS &
   git clone git@github.com:papiro/onboard.git ./.backStage/onboard &
   git clone git@github.com:papiro/grate.css.git ./.backStage/grate.css
else
	echo "Please install Git."
	exit 1
fi

cd $BACKSTAGE/BuddhaJS || cleanup
npm install || { echo "'npm install' failed to install *BuddhaJS* dependencies"; cleanup; }

cd $BACKSTAGE/onboard || cleanup
npm install || { echo "'npm install' failed to install *onboard* dependencies"; cleanup; }

cd $BACKSTAGE/grate.css || cleanup
npm install || { echo "'npm install' failed to install *grate.css* dependencies"; cleanup; }
